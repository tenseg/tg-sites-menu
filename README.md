# Modify "My Sites" Menu
Contributors: eceleste  
Donate link: https://www.tenseg.net  
Tags: admin, multisite, menu  
Requires at least: 5.4  
Tested up to: 5.4.2  
Stable tag: 2.0.0  
Requires PHP: 7.0

Simplifies or hides the "My Sites" menu. Changes it to "Sites" and only shows it if you can manage more than one site. Also modifies it to use [WPS Hide Login](https://wordpress.org/plugins/wps-hide-login/) slugs, if they are present.

## Installation

Clone this repository into `/wp-content/plugins/` or upload the `tg-sites-menu` folder to the `/wp-content/plugins/` directory

Activate the plugin through the 'Plugins' menu in WordPress

## Changelog

### 2.0.0 (2021-02-09)
* accomodate WPS Hide Login slugs, when present
* changed to tg-site-menu

### 2014-06-16
* check for $node->parent == "my-sites-list" rather than "my-sites"

### 2013-11-15
* initial version as efc-hide-my-sites


## Developer Information

### Version Bumps

Please update the following files whenever you need to bump the version. Note, a version bump is essential to having changes in the stylesheet recognized by browsers in the wild.

* [tg-sites-menu.php](tg-sites-menu.php) (change the Version in the header, which is the usual WordPress thing to do)
* in this file replace the Stable tag in the header, add to the Changelog, and update the Upgrade Notice