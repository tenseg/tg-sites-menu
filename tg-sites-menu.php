<?php
/*
 * Plugin Name: Modify "My Sites" Menu
 * Plugin URI: https://bitbucket.org/tenseg/tg-sites-menu
 * Description: Simplifies or hides the "My Sites" menu. Changes it to "Sites" and only shows it if you can manage more than one site. Also modifies it to use WPS Hide Login slugs, if they are present.
 * Version: 2.0.0
 * Author: Tenseg LLC
 * Author URI: https://tenseg.net/
 * License: GNU Lesser GPL 2.1 (http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt)
 * Network: true
 */

if ( is_multisite() ) {

/**
 * Hacks the $wp_admin_bar global in order to correct,
 * simplify, or hide it.
 *
 * Called by action: wp_before_admin_bar_render
 *
 * @return void
 */
	function tg_remove_mysites_menu() {
		global $wp_admin_bar;
		$all_toolbar_nodes = $wp_admin_bar->get_nodes();

		$my_site_kids = 0;

		foreach ( $all_toolbar_nodes as $node ) {
			if ( 'my-sites-list' == $node->parent ) {
				// count the number of accessible subsites
				$my_site_kids++;
			}
			if ( preg_match( '/^blog-(\d+)(.*)/', $node->id, $matches ) ) {
				$blog_id = $matches[1];
				if ( $login_slug = tg_get_wps_hide_login_slug( $blog_id ) ) {
					// since we got a slug, we know that WPS Hide Login is active
					// and we will modify and simplify the menu links as needed
					if ( !$matches[2] || '-d' === $matches[2] ) {
						// this is a link to the site admin for a subsite
						// so we modify the link to use the WPS Hide Login slug
						$args = $node;
						$old_href = $args->href;
						$args->href = preg_replace( '/wp-admin\/$/', "$login_slug/", $old_href );
						if ( $old_href !== $args->href ) {
							$wp_admin_bar->add_node( $args );
						}
					} elseif ( strpos( $node->href, '/wp-admin/' ) !== false ) {
						// we cannot be sure that deep links into the admin side
						// will work when WPS Hide Login is present, so we
						// remove these notes to new post and manage comments
						// which are rarely used anyway
						$wp_admin_bar->remove_node( $node->id );
					}
				}
			}
			if ( 'my-sites' == $node->id ) {
				// shorten the menu title from "My Sites" to "Sites"
				$args = $node;
				$args->title = 'Sites';
				$wp_admin_bar->add_node( $args );
			}
		}

		if ( $my_site_kids < 2 && !current_user_can( 'manage_network' ) ) {
			// if there are not enough accessible subsites, remove the whole menu
			$wp_admin_bar->remove_menu( 'my-sites' );
		}
	}
	add_action( 'wp_before_admin_bar_render', 'tg_remove_mysites_menu' );

/**
 * Returns a login slug if one has been defined
 * by the WPS Hide Login plugin.
 *
 * @param int $blog_id
 * @return string
 */
	function tg_get_wps_hide_login_slug( $blog_id ) {
		if ( defined( 'WPS_HIDE_LOGIN_BASENAME' ) ) {
			if ( $blog_id ) {
				if ( $slug = get_blog_option( $blog_id, 'whl_page' ) ) {
					return $slug;
				}
			} else {
				if ( $slug = get_option( 'whl_page' ) ) {
					return $slug;
				}
			}
			if (  ( is_multisite() && is_plugin_active_for_network( WPS_HIDE_LOGIN_BASENAME ) && ( $slug = get_site_option( 'whl_page', 'login' ) ) ) ) {
				return $slug;
			}
			return 'login';
		}
		return '';
	}

}